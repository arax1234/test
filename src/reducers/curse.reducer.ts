import initialState from '../store/initialState';
import {IAction} from '../interfaces/IAction';
import {GET_CURSE_START, GET_CURSE_ERROR, GET_CURSE_SUCCESS, UPDATE_CURSE_ERROR, UPDATE_CURSE_START, UPDATE_CURSE_SUCCESS} from '../actionTypes/curse.actionType';

import {ICours} from '../interfaces/ICours';

export default function (state: ICours = initialState.course, action: IAction<any>) {
    switch (action.type) {
        case GET_CURSE_START:
            return {
                ...state,
                loading: true,
                data: []
            };
        case GET_CURSE_SUCCESS:
            return {
                ...state,
                loading: false,
                data: action.payload.data
            };
        case GET_CURSE_ERROR:
            return {
                ...state,
                loading: false
            };

        case UPDATE_CURSE_START:
            return {
                ...state,
                loading: true
            };
        case UPDATE_CURSE_SUCCESS:
            return {
                ...state,
                loading: false,
                data: action.payload.data
            };
        case UPDATE_CURSE_ERROR:
            return {
                ...state,
                loading: false
            };
        default:
            return state;
    }
}