import {IRootState} from '../interfaces/IRootState';
import {combineReducers} from 'redux';
import appReducers from './app.reducer';
import profileReducers from './profile.reducer';
import course from './curse.reducer';

const appReducer = combineReducers<IRootState>({
    app: appReducers,
    course: course,
    profile: profileReducers

});
const rootReducer = (state, action) => {

    return appReducer(state, action);
};

export default rootReducer;
