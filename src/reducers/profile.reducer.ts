import {IRootState} from '../interfaces/IRootState';
import {
    SAVE_PROFILE_ERROR,
    SAVE_PROFILE_START,
    SAVE_PROFILE_SUCCESS,
    INIT_PROFILE_ERROR,
    INIT_PROFILE_START,
    INIT_PROFILE_SUCCESS
} from '../actionTypes/profile.actionTypes';
import {IAction} from '../interfaces/IAction';
import initialState from '../store/initialState';
import {IProfile} from '../interfaces/IProfile';

export default function (state: IProfile = initialState.profile, action: IAction<any>) {

    switch (action.type) {
        case SAVE_PROFILE_START:
            return {
                ...state,
                loading: true
            };
        case SAVE_PROFILE_SUCCESS:
            return {
                ...state, ...action.payload,
                organizations: [],
                paging: 1,
                totalOrganization: 0,
                loading: false
            };
        case SAVE_PROFILE_ERROR:
            return {
                ...state,
                loading: false
            };

        case INIT_PROFILE_START:
            return {
                ...state,
                loading: true
            };
        case INIT_PROFILE_SUCCESS:
            return {
                ...state,
                ...action.payload,
                loading: false
            };
        case INIT_PROFILE_ERROR:
            return {
                ...state,
                loading: false
            };
        default:
            return state;
    }
}