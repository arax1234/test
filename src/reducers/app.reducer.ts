import { IRootState } from '../interfaces/IRootState';
import {
    APP_LOAD,
    INIT_REALM_START,
    INIT_REALM_SUCCESS,
    INIT_REALM_ERROR,
    VALIDATE_INTERNET_START,
    VALIDATE_INTERNET_SUCCESS,
    VALIDATE_INTERNET_ERROR,
    SET_SERVER_START,
    SET_SERVER_SUCCESS,
    SET_SERVER_ERROR,
    INIT_SERVER_START,
    INIT_SERVER_SUCCESS,
    INIT_SERVER_ERROR,
    SET_STAGE_ERROR,
    SET_STAGE_START,
    SET_STAGE_SUCCESS
} from '../actionTypes/app.actionTypes';
import { IAction } from '../interfaces/IAction';
import initialState from '../store/initialState';

export default function (state: IRootState = initialState, action: IAction<any>) {
    switch (action.type) {

        case APP_LOAD:
            return { ...state, loading: false };

        case INIT_REALM_START:
            return { ...state };

        case INIT_REALM_SUCCESS:
            return { ...state, realm: action.payload };

        case INIT_REALM_ERROR:
            return { ...state };

        case VALIDATE_INTERNET_START:
            return { ...state };

        case VALIDATE_INTERNET_SUCCESS:
            return { ...state, internet: action.payload };

        case VALIDATE_INTERNET_ERROR:
            return { ...state };
        case SET_SERVER_START:
            return {
                ...state,
            };

        case SET_SERVER_SUCCESS:
            return { ...state, server: action.payload };

        case SET_SERVER_ERROR:
            return {
                ...state
            };
        case INIT_SERVER_START:
            return {
                ...state
            };

        case INIT_SERVER_SUCCESS:
            return { ...state, server: action.payload };

        case INIT_SERVER_ERROR:
            return {
                ...state
            };
        case SET_STAGE_START:
            return {
                ...state
            };
        case SET_STAGE_SUCCESS:
            return { ...state, stage: action.payload };

        case SET_STAGE_ERROR:
            return {
                ...state
            };
        default:
            return state;
    }
}