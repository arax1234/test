//const server = 'http://eis3.lanit.ru/';
const server = 'http://eis3.lanit.ru/api/mobile/';
//const server = 'http://fks2.lanit.ru/';
//const server = 'http://zakupki.gov.ru/';
const ServerZakupkiGov = 'http://zakupki.gov.ru/';
const shareServer = 'http://eis3.lanit.ru/';
const newServer = 'http://eis3.lanit.ru/api/mobile/';

//http://eis3.lanit.ru/api/mobile/orders/analytics/actual-competitor?mainBidderId=1795135&sortDirection=DESC&sortProperties=ALL
export default class R {
    static server = 'http://zakupki.gov.ru/';
    // public setServer(server: string){
    //   this.server1 = server;
    // };
    static string = {

        tabsNames: {
            home: 'Курсы',
            cabinet: 'Кабинет',
            settings: 'Настройки',
            course: 'Курсы валют',


            bookmarks: 'Закладки',

            favorites: 'Избранное',
            analytics: 'Аналитика',
            main: 'Новое',
            profile: 'Кабинет',
            another: 'Другое'
        },






    };

    static image = {
        new_search_light: require('../../img/new/search_light.png'),
        new_search_dark: require('../../img/new/search_dark.png'),
        new_search_new_blue: require('../../img/new/search_new_blue.png'),
        new_bookmark_dark: require('../../img/new/bookmark_dark.png'),
        new_bookmark_light: require('../../img/new/bookmark_light.png'),
        new_bookmark_white: require('../../img/new/bookmark_white.png'),
        new_bookmark_blue: require('../../img/new/bookmark_blue.png'),
        new_chartBar_dark: require('../../img/new/chart-bar_dark.png'),
        new_chartBar_blue: require('../../img/new/chart-bar_blue.png'),
        new_close_blue: require('../../img/new/close_blue.png'),
        new_slidersH_dark: require('../../img/new/sliders-h_dark.png'),
        new_star_dark: require('../../img/new/star_dark.png'),
        new_star_blue: require('../../img/new/star_blue.png'),
        new_user_dark: require('../../img/new/user_dark.png'),
        new_user_blue: require('../../img/new/user_blue.png'),
        new_close_dark: require('../../img/new/close_dark.png'),
        new_flag_red: require('../../img/new/flag_red.png'),
        new_flag_blue: require('../../img/new/flag_blue.png'),
        new_check_light: require('../../img/new/check_light.png'),
        new_check_white: require('../../img/new/check_white.png'),
        new_ruble_blue: require('../../img/new/ruble_blue.png'),
        new_barcode_blue: require('../../img/new/barcode_blue.png'),
        new_tag_blue: require('../../img/new/tag_blue.png'),
        new_tag_white: require('../../img/new/tag_white.png'),
        new_clock_blue: require('../../img/new/clock_blue.png'),
        new_shoppingBasket_blue: require('../../img/new/shopping-basket_blue.png'),
        new_idCard_blue: require('../../img/new/id-card_blue.png'),
        new_calendar_blue: require('../../img/new/calendar_blue.png'),
        new_calendar_white: require('../../img/new/calendar_white.png'),
        new_userDesk_blue: require('../../img/new/user-desk_blue.png'),
        new_mapMarker_blue: require('../../img/new/map-marker_blue.png'),
        new_mapMarker_white: require('../../img/new/map-marker_white.png'),
        new_chevronRight_blue: require('../../img/new/chevron-right_blue.png'),
        new_chevronLeft_blue: require('../../img/new/chevron-left_blue.png'),
        new_sort_dark: require('../../img/new/sort_dark.png'),
        new_sort_dark_rounded: require('../../img/new/sort_dark_rounded.png'),
        new_setting: require('../../img/new/settings.png'),
        new_folderMinus_blue: require('../../img/new/folder-minus_blue.png'),
        new_favorites_on: require('../../img/new/favorites-on.png'),
        new_favorites_off: require('../../img/new/favorites-off.png'),
        new_alarm_blue: require('../../img/new/alarm_blue.png'),
        new_alarm_dark: require('../../img/new/alarm_dark.png'),
        new_alarm_light: require('../../img/new/alarm_light.png'),
        new_alarm_violet: require('../../img/new/alarm_violet.png'),
        new_edit_blue: require('../../img/new/edit_blue.png'),
        new_edit_dark: require('../../img/new/edit_dark.png'),
        new_share_dark: require('../../img/new/share_dark.png'),
        new_chevronBottom_blue: require('../../img/new/chevron-bottom_blue.png'),
        new_screen_1: require('../../img/new/screen_1.png'),
        new_screen_2: require('../../img/new/screen_2.png'),
        new_screen_3: require('../../img/new/screen_3.png'),
        new_screen_4: require('../../img/new/screen_4.png'),
        new_chevronTop_blue: require('../../img/new/chevron-top_blue.png'),
        new_share_blue: require('../../img/new/share_blue.png'),
        new_wallet: require('../../img/new/wallet_blue.png'),
        new_alarm_clock_blue: require('../../img/new/alarm_clock_blue.png'),
        new_support_blue: require('../../img/new/support_blue.png'),
        new_contact_blue: require('../../img/new/contact_blue.png'),
        new_folder_blue: require('../../img/new/folder_blue.png'),
        new_message_blue: require('../../img/new/message_blue.png'),
        new_go_browser_blue: require('../../img/new/go_browser_blue.png'),
        new_gerb: require('../../img/new/gerb.png'),
        new_launchScreenGerb: require('../../img/new/launchScreenGerb.png'),
        new_phone_blue: require('../../img/new/phone_blue.png'),
        new_faks_blue: require('../../img/new/faks_blue.png'),
        new_sberbank: require('../../img/new/sberBank.png'),
        new_roseltorg: require('../../img/new/roseltorg.png'),
        new_zakazrf: require('../../img/new/zakazrf.png'),
        new_rtstander: require('../../img/new/rtstander.png'),
        new_etpets: require('../../img/new/etpets.png'),
        new_gzlotonline: require('../../img/new/gzlotonline.png'),
        new_tektorg: require('../../img/new/tektorg.png'),
        new_etpgpb: require('../../img/new/etpgpb.png'),
        new_astgoz: require('../../img/new/astgoz.png'),
        new_launchScreenImage: require('../../img/new/LaunchScreenImage.png'),
        new_launchScreenText: require('../../img/new/LaunchScreenText.png'),
        new_pencil_white: require('../../img/new/pencil_white.png'),
        new_shareSquare_white: require('../../img/new/share-square_white.png'),
        new_trash_white: require('../../img/new/trash_white.png'),
        new_ellipse_blue: require('../../img/new/ellipse_blue.png'),
        new_welcomeCircles: require('../../img/new/welcom_circles.png'),
        new_welcomeCirclesFull: require('../../img/new/welcom_circles_full.png'),
        new_parallaxShineViolet: require('../../img/new/parallax_shine_violet.png'),
        new_parallaxShineBlue: require('../../img/new/parallax_shine_blue.png'),
        new_parallaxLetterWhite: require('../../img/new/parallax_letter_white.png'),
        new_parallaxFolderBlue: require('../../img/new/parallax_folder_blue.png'),
        new_parallaxDocumentBlue: require('../../img/new/parallax_document_blue.png'),
        new_parallaxClockBlue: require('../../img/new/parallax_clock_blue.png'),
        new_parallaxSearchBlue: require('../../img/new/parallax_search_blue.png'),
        new_sort_up: require('../../img/new/sort_up.png'),
        new_sort_down: require('../../img/new/sort_down.png'),
        new_parallaxBookmarkBlue: require('../../img/new/parallax_bookmark_blue.png'),
        new_parallaxClockBigBlue: require('../../img/new/parallax_clock_big_blue.png'),
        new_parallaxClockFilledBlue: require('../../img/new/parallax_clock_filled_blue.png'),
        new_parallaxFlagBlue: require('../../img/new/parallax_flag_blue.png'),
        new_parallaxLockBlue: require('../../img/new/parallax_lock_blue.png'),
        new_parallaxStarBlue: require('../../img/new/parallax_star_blue.png'),
        new_parallaxStarFlagBlue: require('../../img/new/parallax_star_flag_blue.png'),
        new_parallaxSuitcaseBlue: require('../../img/new/parallax_suitcase_blue.png'),


    };

    static color = {
        //new style
        new_violet: '#751AEA',
        new_blue: '#315DFA',
        new_hover_blue: '#501AEA',
        new_light_blue: '#62ADE9',
        new_light: '#6F7C98',
        new_extra_light: '#A6B1C6',
        new_line_base: '#DAE4EE',
        new_line_light: '#E4E7F2',
        new_background: '#EFF0F2',
        new_white: '#FFFFFF',
        new_base_dark: '#27344C',
        new_green: '#99CC67',
        new_grass: '#C3EA9D',
        new_red: '#E8523D',
        new_pink: '#F1BBAF',
        new_yellow: '#F2E084',
        new_sea: '#8BDCE1',
        new_shadowColor: '#2D85ED'
    };

    public static url = {
        shareServer: shareServer
    };
}

export interface SortMethod {
    key: string;
    title: string;
}
