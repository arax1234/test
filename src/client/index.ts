/**
 * обработчик ошибок
 */

export function dataError(type: string, err: any) {


    return dispatch => {
        dispatch({ type });
    };
}

interface IDataSuccess {
    type: string;
    payload?: any;
}

interface IExtData {
    params?: any;
    action?();
}

export function dataSuccess (data: IDataSuccess, extData: IExtData = {params: null, action: null}) {

    return dispatch => {
        dispatch(data);
    };
}