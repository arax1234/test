/**
 * базовый интерфейс - использется на экранах с навигатором
 */
import { Navigator } from 'react-native-navigation';

export interface ViewWithNavigator {
    navigator: Navigator;
}