/**
 * Прелодер с выводом ошибок (Отсутствия интернета и Неудачной попытки получить данные с сервера)
 * При нажатии повторить попытку пытается обновить данные списка и карточек закупок
 */
import React, {Component} from 'react';
import {View, ActivityIndicator, Text, TouchableOpacity, ViewStyle, TextStyle} from 'react-native';

import Redux, {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import R from '../resources/R';

import {IAction} from '../interfaces/IAction';
import {IRootState} from '../interfaces/IRootState';

interface IProps {
    internet: boolean;
    simple?: boolean; // если true не выводит ошибки
}

interface IState {
    notAllow: boolean;
}

class Preloader extends Component<IProps, IState> {
    timerHandle: any;

    constructor(props: IProps) {
        super(props);

        this.state = {
            notAllow: false
        };

        this.timerHandle = null;
    }

    componentWillReceiveProps(nextProps: IProps) {
        if (nextProps.internet && this.props.internet !== nextProps.internet) {
            this.handleAction();
        }
    }

    componentWillUnmount() {
        if (this.timerHandle) {
            clearTimeout(this.timerHandle);
            this.timerHandle = null;
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator
                    hidesWhenStopped={true}
                    color={'#000'}
                    style={{flex: 1}}
                    animating={true}
                />
            </View>
        );
    }

    private reload() {
        this.setState({notAllow: false}, () => {
            this.timeLimit();
        });
    }

    private timeLimit() {
        if (this.timerHandle) {
            clearTimeout(this.timerHandle);
            this.timerHandle = null;
        }

        this.timerHandle = setTimeout(() => {
            this.setState({notAllow: true});
        }, 20000);
    }

    private handleAction() {
        this.reload();
    }


}

function mapStateToProps(state: IRootState) {

    return {
        internet: state.app.internet
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<IAction<any>>) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(Preloader);

class Styles {
    container: ViewStyle = {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        backgroundColor: 'transparent'
    };

    textContainer: ViewStyle = {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 10
    };

    text: TextStyle = {
        fontFamily: 'IBM Plex Sans',
        fontWeight: '400',
        fontStyle: 'normal',
        paddingHorizontal: 50,
        textAlign: 'center',
        marginBottom: 20
    };
}

const styles = new Styles();
