import {Navigation} from 'react-native-navigation';
import {Provider} from 'react-redux';
import {registerScreens} from './screens/Screens';
import configureStore from './store/configureStore';
import initialState from './store/initialState';
import R from './resources/R';

export const store = configureStore(initialState);

function getApp() {
    console.disableYellowBox = true;
    Navigation.startTabBasedApp({
        tabs: [
            {
                label: R.string.tabsNames.cabinet,
                screen: 'uis.CabinetView',
                icon: R.image.new_user_dark,
                selectedIcon: R.image.new_user_blue,
                renderAsOriginal: true,
                title: R.string.tabsNames.cabinet,
                tabBarHideShadow: true
            },
            {
                label: R.string.tabsNames.home,
                screen: 'uis.CourseView',
                title: R.string.tabsNames.course,
                icon: R.image.new_search_dark,
                selectedIcon: R.image.new_search_new_blue,
                renderAsOriginal: true
            }
        ],
        tabsStyle: { // optional, add this if you want to style the tab bar beyond the defaults
            initialTabIndex: 0, // optional, the default selected bottom tab. Default: 0. On Android, add this to appStyle
            tabBarBackgroundColor: '#FFFFFF',
            tabBarTextFontFamily: 'IBM Plex Sans',
            tabBarLabelColor: R.color.new_base_dark,
            tabBarSelectedLabelColor: R.color.new_blue,
            tabBarHideShadow: true
        },
        animationType: 'fade'
    });

}

async function startApp() {
    try {

        getApp();
    } catch (error) {
    }
}

export default async function () {
    registerScreens(store, Provider, 0);

    await startApp();
}
