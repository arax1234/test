class Settings {
    static schema = {
        name: 'Settings',
        primaryKey: 'key',
        properties: {
            key: 'string',
            value: 'string'
        }
    };
}

export default Settings;