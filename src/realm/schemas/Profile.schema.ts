export default class Profile {
    public static schema: any = {
        name: 'Profile',
        properties: {
            currencies: 'string'
        }
    };
}