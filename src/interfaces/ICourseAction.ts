export interface ICourseAction {
    getCurrentCourse();
    updateCurrentCourse();
}
