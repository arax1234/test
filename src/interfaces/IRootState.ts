import { IApp } from './IApp';
import { IProfile } from './IProfile';
import {ICours} from './ICours';

export interface IRootState {
    app: IApp;
    course: ICours;
    profile: IProfile;

}
