export interface IAppActions {
    initRealm();

    setServer(server: string);

    setStage(stage: number);
}