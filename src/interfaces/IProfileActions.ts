import { IProfile } from './IProfile';
import { IRootState } from './IRootState';

export  interface IProfileActions {
    initProfile();
    saveProfile(profile: IProfile);
}