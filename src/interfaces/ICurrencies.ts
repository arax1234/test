export interface ICurrencies {
    data: any[];
    defaultCurrencies: string;
    loading: boolean;
}