import Realm from 'realm';

export interface IApp {
    internet: boolean;
    loading: boolean;
    realm?: Realm;
    server: string;
    stage: number;
}