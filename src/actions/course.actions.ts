import {GET_CURSE_ERROR, GET_CURSE_START, GET_CURSE_SUCCESS} from '../actionTypes/curse.actionType';
import axios from 'axios';

export function getCurrentCourse() {
    return async (dispatch) => {
        dispatch({type: GET_CURSE_START});
        try {
            const response = await axios.get('https://www.cbr-xml-daily.ru/daily_json.js');
            if (response.data) {
                const array = Object.keys(response.data.Valute).map((item) => {
                    return response.data.Valute[item];
                });

                const obj = {
                    CharCode: 'RUB',
                    Nominal: 1,
                    ID: 'R00000',
                    Name: 'Российский рубль',
                    Previous: 1,
                    Value: 1
                }
                array.push(obj);
                dispatch({
                    type: GET_CURSE_SUCCESS,
                    payload: {data: array}
                });
            }
        } catch (e) {
            dispatch(GET_CURSE_ERROR, e);
        }
    };
}