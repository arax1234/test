import {
    INIT_REALM_START,
    INIT_REALM_SUCCESS,
    INIT_REALM_ERROR,
    VALIDATE_INTERNET_START,
    VALIDATE_INTERNET_ERROR,
    VALIDATE_INTERNET_SUCCESS,
    SET_SERVER_START,
    SET_SERVER_SUCCESS,
    SET_SERVER_ERROR,
    INIT_SERVER_START,
    INIT_SERVER_SUCCESS,
    INIT_SERVER_ERROR,
    SET_STAGE_ERROR,
    SET_STAGE_START,
    SET_STAGE_SUCCESS
} from '../actionTypes/app.actionTypes';
import R from '../resources/R';
import { IAction } from '../interfaces/IAction';
import Realm from 'realm';
import schemas from '../realm/schemas';
import { initProfile } from './profile.actions';
import { NetInfo } from 'react-native';
import _ from 'lodash';
import {getCurrentCourse} from './course.actions';
import {dataError} from '../client';

// подключение Realm
export function setRealm(realm: Realm): IAction<any> {
    return {
        type: INIT_REALM_SUCCESS,
        payload: realm
    };
}

// инициализация данных приложения
export function initRealm() {
    return dispatch => {
        dispatch({ type: INIT_REALM_START });

        Realm.open({
            deleteRealmIfMigrationNeeded: true,
            schema: schemas,
            schemaVersion: 1
        })
            .then(realm => {
                dispatch(setRealm(realm));
                dispatch(initServer());
                dispatch(validateInternet());
                dispatch(getCurrentCourse());
                dispatch(initProfile());
            })
            .catch((e) => {
                dispatch(dataError(INIT_SERVER_ERROR, e));
            });
    };
}

// проверка подключения к сети интернет
export function validateInternet() {
    return dispatch => {
        setTimeout(() => {
            NetInfo.isConnected.addEventListener(
                'connectionChange',
                handleFirstConnectivityChange
            );
        }, 500);

        function handleFirstConnectivityChange(isConnected: boolean) {
            dispatch({ type: VALIDATE_INTERNET_START });

            try {
                dispatch({ type: VALIDATE_INTERNET_SUCCESS, payload: isConnected });
            } catch (e) {
                dispatch(dataError(VALIDATE_INTERNET_ERROR, e));
            }
        }
    };
}
export function initServer() {
    return (dispatch, getState) => {
        const state = getState();
        const realm = state.app.realm;
        dispatch({type: INIT_SERVER_START});

        try {
            const settings = _.values(realm.objects('Settings')).map(item => {
                return {...item};
            });
            let setting = (settings as any[]).find((item) => {
                return item.key === 'server';
            });

            if (!setting) {
                setting = {key: 'server', value: R.server};
            }

            dispatch({
                type: INIT_SERVER_SUCCESS,
                payload: setting.value
            });
        } catch (e) {
            dispatch(dataError(INIT_SERVER_ERROR, e));
        }
    };
}
export function setServer(server: string) {
    return async (dispatch, getState) => {
        const state = getState();

        const realm: Realm = state.app.realm;
        dispatch({type: SET_SERVER_START});

        try {
            realm.write(() => {
                realm.deleteAll();
            });
            realm.write(() => {
                realm.create('Settings', {key: 'server', value: server});
            });
            dispatch({type: 'RESET_APP'})
            dispatch(initRealm())

            dispatch({type: SET_SERVER_SUCCESS, payload: server});

        } catch (e) {
            dispatch(dataError(SET_SERVER_ERROR, e));
        }
    };
}
export function setStage(stage: number) {
    return async (dispatch, getState) => {
        const state = getState();

        dispatch({type: SET_STAGE_START});

        try {

            dispatch({type: SET_STAGE_SUCCESS, payload: stage});

        } catch (e) {
            dispatch(dataError(SET_STAGE_ERROR, e));
        }
    };
}
export function tabReset(stage: number) {
    return async (dispatch, getState) => {
        const state = getState();

        dispatch({type: SET_STAGE_START});

        try {

            dispatch({type: SET_STAGE_SUCCESS, payload: stage});

        } catch (e) {
            dispatch(dataError(SET_STAGE_ERROR, e));
        }
    };
}