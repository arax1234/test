import {
    SAVE_PROFILE_START,
    SAVE_PROFILE_ERROR,
    INIT_PROFILE_START,
    INIT_PROFILE_SUCCESS,
    INIT_PROFILE_ERROR
} from '../actionTypes/profile.actionTypes';
import {IProfile} from '../interfaces/IProfile';
import {IRootState} from '../interfaces/IRootState';

export function saveProfile(profile: IProfile) {
    return (dispatch, getState: () => IRootState) => {
        dispatch({type: SAVE_PROFILE_START});
        try {
            realmSave(profile, getState);
            dispatch(initProfile());
        } catch (e) {
            dispatch(SAVE_PROFILE_ERROR, e);
        }
    };

    function realmSave(profile: IProfile, getState: () => IRootState) {
        const realm = getState().app.realm;
        const profiles = realm.objects('Profile');
        const profileItem = profiles[0];

        const newProfile = {
            currencies: profile.currencies
        };

        try {
            realm.write(() => {
                if (profileItem) {
                    realm.delete(profileItem);
                }
                realm.create('Profile', newProfile);
            });
        } catch (e) {
            // console.error(e);
        }
    }
}

export function initProfile() {
    return (dispatch, getState: () => IRootState) => {
        dispatch({type: INIT_PROFILE_START});
        try {
            dispatch({
                type: INIT_PROFILE_SUCCESS,
                payload: loadProfile(getState)
            });
        } catch (e) {
            dispatch(INIT_PROFILE_ERROR, e);
        }
    };

    function loadProfile(getState: () => IRootState): any {
        const state = getState();
        const realm = state.app.realm;
        const profiles = realm.objects('Profile');
        const profileItem = profiles[0];
        if (profileItem) {
            return profileItem;
        }
        const pr = state.profile;
        realm.write(() => {
            realm.create('Profile', pr);
        });

        return state.profile;
    }
}