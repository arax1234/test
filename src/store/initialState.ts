import {IRootState} from '../interfaces/IRootState';
import R from '../resources/R';

const initialState: IRootState = {
    app: {
        internet: true,
        loading: true,
        realm: undefined,
        server: R.server,
        stage: 0
    },

    course: {
        data: [],
        loading: true
    },
    profile: {
        currencies: 'RUB'
    }
};
export default initialState;