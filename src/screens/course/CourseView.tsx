import * as React from 'react';
import {Text, View, TextInput} from 'react-native';
import {Navigator} from 'react-native-navigation';
import Redux, {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {styles} from './CourseView.styles';
import R from './../../resources/R';

import {IAction} from '../../interfaces/IAction';
import {IProfile} from '../../interfaces/IProfile';

import {IApp} from '../../interfaces/IApp';
import {ICours} from '../../interfaces/ICours';
import {CurrencyFilterDetailViewNew} from '../settings/currency/CurrencyFilterDetailsViewNew';
import {IRootState} from '../../interfaces/IRootState';

interface IProps {
    navigator: Navigator;
    profile: IProfile;
    app: IApp;
    course: ICours;
}

interface IState {
    course: ICours;
    selectedValue: string;
    changeValue: string;
}

const buttons = {
    CANCEL: 'cancel',
    BOOKMARKS: 'bookmarks'
};

class CourseView extends React.Component<IProps, IState> {

    static navigatorStyle: any = {
        navBarTextColor: R.color.new_base_dark,
        navBarBackgroundColor: R.color.new_background,
        navBarNoBorder: true,
        navBarTextFontFamily: 'IBM Plex Sans',
        navBarTextFontSize: 22,
        navBarButtonColor: R.color.new_blue
    };

    onNavigatorEvent(event: any) {

    }

    constructor(props: IProps) {
        super(props);
        this.state = {
            course: this.props.course,
            selectedValue: '',
            changeValue: ''
        };
        props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    componentWillReceiveProps(pr: IProps) {
        if (pr.course.data.length > 0) {
            this.setState({course: pr.course, selectedValue: pr.course.data[0].CharCode});
        }
    }

    private onValueChanged(value: any) {
        this.setState({selectedValue: value});
    }

    onChangeValue(value: any) {
        this.setState({changeValue: value});
    }

    render() {
        const {course, profile} = this.props;
        let sum = 0;
        if (course.data.length > 0) {
            const current = course.data.find(it => it.CharCode === this.state.selectedValue);
            sum = this.state.changeValue ? current.Value * parseInt(this.state.changeValue) : 0;
        }
        const arr = [];
        course.data.forEach(it => {
            if (it.CharCode !== profile.currencies) {
                arr.push(it);
            }
        });

        return (
            course.data.length > 0 &&
            (<View style={styles.container}>

                <View style={styles.rowContainer}>
                    <Text>{profile.currencies}</Text>
                    <CurrencyFilterDetailViewNew
                        currencies={arr}
                        selectedCurrency={this.state.selectedValue}
                        isNotChange={false}
                        onValueChanged={value => this.onValueChanged(value)}
                    />
                </View>
                <View style={styles.rowContainer}>
                    <TextInput keyboardType={'number-pad'} style={styles.inputText} value={this.state.changeValue} onChangeText={(value) => this.onChangeValue(value)}></TextInput>
                </View>
                <View style={[styles.rowContainer, {justifyContent: 'center'}]}>
                    <Text>{
                        sum.toFixed(4)
                    }</Text>
                </View>
            </View>)
        );
    }

}

function mapStateToProps(state: IRootState) {
    return {
        profile: state.profile,
        app: state.app,
        course: state.course
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<IAction<any>>) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(CourseView);
