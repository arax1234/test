import {ViewStyle, TextStyle, ImageStyle, Dimensions} from 'react-native';
import R from '../../resources/R';

const {height, width} = Dimensions.get('window')

class Styles {

    container: ViewStyle = {
        backgroundColor: R.color.new_background,
        paddingTop: 20,
        paddingHorizontal: 20,
        flexDirection: 'column',
        height: height
    };
    rowContainer: ViewStyle = {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 20
    };

    inputText: TextStyle = {
        height: 40,
        width: width - 40,
        borderColor: R.color.new_extra_light,
        borderWidth: 1,
        borderRadius: 10,
        paddingHorizontal: 20,
        backgroundColor: R.color.new_white,
        color: R.color.new_light
    };

}

export const styles = new Styles();
