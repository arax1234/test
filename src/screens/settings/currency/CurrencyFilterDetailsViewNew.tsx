import React, {Component} from 'react';
import {Dimensions, ImageStyle, Modal, Picker, Text, TextStyle, TouchableOpacity, View, ViewStyle, Image} from 'react-native';
import R from '../../../resources/R';

interface IProps {

    currencies: any[];
    isNotChange?: boolean;

    onValueChanged(newValue: string);

    selectedCurrency: string | undefined;
}

interface IState {
    opened: boolean;
    selectedValue: string;
}

export class CurrencyFilterDetailViewNew extends Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            opened: false,
            selectedValue: this.props.selectedCurrency

        };
    }


    public render() {
        const {currencies, selectedCurrency} = this.props;
        const currency = currencies.find((item) => item.CharCode.toString() === selectedCurrency);
        const currencyName = currency ? currency.Name : '';
        return [
            <TouchableOpacity style={styles.container} onPress={() => this.setState({opened: !this.state.opened})}>
                <View style={styles.subContainer}>
                    {/*<View style={styles.iconContainer}>*/}
                    {/*<Image style={styles.icon} source={R.image.new_ruble_blue}/>*/}
                    {/*</View>*/}
                    {this.props.isNotChange ?
                        [
                            <Text style={styles.header}>{'Валюта'}</Text>,

                            <View style={styles.descriptionContainer}>
                                {

                                    (
                                        <Text style={styles.descriptionBlack} numberOfLines={1}>{currencyName}</Text>
                                    )
                                }
                            </View>,
                            <Image
                                style={styles.chevronImage}
                                source={R.image.new_chevronRight_blue}
                            />
                        ] : [
                            <View style={styles.descriptionContainer}>
                                <Text style={styles.descriptionBlack} numberOfLines={1}>{selectedCurrency}</Text>
                            </View>,
                            <Image
                                style={styles.chevronImage}
                                source={R.image.new_chevronBottom_blue}
                            />
                        ]
                    }
                </View>
            </TouchableOpacity>,
            <Modal
                animationType='fade'
                transparent={true}
                visible={this.state.opened}
            >
                <View style={styles.modalBackground}
                      onTouchStart={
                          () => this.setState({opened: !this.state.opened},
                              this.props.onValueChanged(this.state.selectedValue ? this.state.selectedValue.toString() : null))}
                />
                <View style={styles.modalContainer}>
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity style={styles.leftHeader} onPress={() => {

                        }}/>
                        <TouchableOpacity style={styles.rightHeader} onPress={() => {
                            this.setState({opened: !this.state.opened},
                                this.props.onValueChanged(this.state.selectedValue ? this.state.selectedValue.toString() : null));
                        }}>
                            <Text style={styles.okText}>Готово</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.modalList}>
                        <Picker
                            selectedValue={this.state.selectedValue}
                            onValueChange={(value) => {
                                this.setState({selectedValue: value ? value.toString() : null});

                            }}>
                            {
                                [
                                    currencies.map(
                                        item => (
                                            <Picker.Item key={item.Id} label={`${item.Name}`} value={item.CharCode ? item.CharCode : null}/>
                                        ))
                                ]
                            }
                        </Picker>
                    </View>
                </View>
            </Modal>
        ];
    }
}

const {width} = Dimensions.get('window');

class Styles {
    container: ViewStyle = {
        marginHorizontal: 20,
        paddingVertical: 20,
        borderBottomWidth: 0.5,
        borderBottomColor: R.color.new_line_base
    };
    subContainer: ViewStyle = {
        flexDirection: 'row',
        alignItems: 'center'
    };
    iconContainer: ViewStyle = {
        width: 30
    };
    icon: ImageStyle = {};
    header: TextStyle = {
        fontFamily: 'IBM Plex Sans',
        fontWeight: '400',
        fontStyle: 'normal',
        fontSize: 15,
        color: R.color.new_base_dark
    };
    descriptionContainer: ViewStyle = {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginRight: 18,
        flex: 1,
        paddingLeft: 10
    };
    description: TextStyle = {
        fontFamily: 'IBM Plex Sans',
        fontWeight: '400',
        fontStyle: 'normal',
        fontSize: 15,
        color: R.color.new_light
    };
    descriptionBlack: TextStyle = {
        fontFamily: 'IBM Plex Sans',
        fontWeight: '400',
        fontStyle: 'normal',
        fontSize: 15,
        color: R.color.new_base_dark
    };
    chevronImage: ImageStyle = {};
    dateLabel: TextStyle = {
        fontFamily: 'IBM Plex Sans', fontWeight: '400', fontStyle: 'normal',
        marginTop: 12,
        marginBottom: 12
    };
    dateText: TextStyle = {
        fontFamily: 'IBM Plex Sans',
        fontWeight: '400',
        fontStyle: 'normal',
        height: 23,
        justifyContent: 'flex-start',
        fontSize: 17
    };
    dateIcon: ViewStyle = {
        position: 'absolute',
        left: 0,
        top: 4,
        marginLeft: 0,
        borderWidth: 0,
        display: 'none'
    };

    dateInput: TextStyle = {
        fontFamily: 'IBM Plex Sans',
        fontWeight: '400',
        fontStyle: 'normal',
        borderWidth: 0,
        height: 23,
        justifyContent: 'flex-start',
        flexDirection: 'row'
    };

    chevron: ImageStyle = {
        width: 18,
        height: 18,
        tintColor: R.color.new_light
    };

    public priceContainer: ViewStyle = {
        flexDirection: 'row'
    };
    public list: ViewStyle = {
        flex: 1
    };

    public fieldItem: ViewStyle = {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '50%',
        paddingRight: 8
    };
    public singleHeader: TextStyle = {
        fontFamily: 'IBM Plex Sans',
        fontWeight: '400',
        fontStyle: 'normal',
        fontSize: 17
    };

    public priceLabel: TextStyle = {
        fontFamily: 'IBM Plex Sans',
        fontWeight: '400',
        fontStyle: 'normal',
        fontSize: 15,
        color: R.color.new_light,
        marginRight: 3
    };
    public priceLabelBlue: TextStyle = {
        fontFamily: 'IBM Plex Sans',
        fontWeight: '400',
        fontStyle: 'normal',
        fontSize: 15,
        color: R.color.new_light,
        marginRight: 3
    };

    public priceInput: TextStyle = {
        fontFamily: 'IBM Plex Sans',
        fontWeight: '400',
        fontStyle: 'normal',
        fontSize: 17,
        borderBottomWidth: 0.5,
        borderBottomColor: '#C8C7CC',
        flex: 2,
        paddingRight: 21,
        paddingLeft: 10,
        paddingVertical: 3
        // height: 34
    };
    public clear: ViewStyle = {
        position: 'absolute',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        top: 0,
        right: 0,
        width: 29,
        height: 29
    };
    public clearImage: ImageStyle = {
        width: 16,
        height: 16,
        marginRight: 8,
        marginBottom: 4
    };

    modalBackground: ViewStyle = {
        position: 'absolute',
        backgroundColor: R.color.new_base_dark,
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        opacity: 0.5
    };
    modalContainer: ViewStyle = {
        position: 'absolute',
        bottom: 0
    };
    buttonContainer: ViewStyle = {
        width: width,
        backgroundColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 15,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        borderBottomColor: R.color.new_line_base,
        borderBottomWidth: 1,
        borderTopColor: R.color.new_line_base,
        borderTopWidth: 1
    };
    leftHeader: ViewStyle = {
        width: width - 100
    };
    rightHeader: ViewStyle = {
        width: 60
    };
    modalList: ViewStyle = {
        width: width,
        backgroundColor: 'white'
    };
    okText: TextStyle = {
        fontFamily: 'IBM Plex Sans',
        fontWeight: '500',
        fontStyle: 'normal',
        color: R.color.new_blue,
        fontSize: 15
    };
}

const styles = new Styles();