import React, {Component} from 'react';
import {Text, View, TextInput, ScrollView, TouchableOpacity, Image, Modal, StatusBar, Picker, FlatList, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import Redux, {bindActionCreators} from 'redux';

import {phonecall} from 'react-native-communications';

import {styles} from './SettingsView.styles';
import R from '../../resources/R';


import {IRootState} from '../../interfaces/IRootState';
import {IAction} from '../../interfaces/IAction';
import {IProfile} from '../../interfaces/IProfile';
import {IProfileActions} from '../../interfaces/IProfileActions';
import {IAppActions} from '../../interfaces/IAppActions';
import {ViewWithNavigator} from '../../components/ViewWithNavigator';

import {IApp} from '../../interfaces/IApp';
import {ICours} from '../../interfaces/ICours';
import {CurrencyFilterDetailViewNew} from './currency/CurrencyFilterDetailsViewNew';
import * as profileActions from '../../actions/profile.actions';


interface IProps extends ViewWithNavigator {
    actions: IProfileActions;
    profile: IProfile;
    appActions: IAppActions;
    app: IApp;
    course: ICours;
}

interface IState {
    anotherScreenOpen: boolean;
    profile: IProfile;
    isShowPicker: boolean;
    modalVisible: boolean;
    serverText: string;
    clickCounter: number;
    lastUpdate: string;
    devModalVisible: boolean;
}

const buttons = {
    BACK: 'back'
};

const rightButton = {
    icon: R.image.new_chevronLeft_blue,
    title: '',
    id: buttons.BACK
};

class SettingsView extends Component<IProps, IState> {
    public static navigatorButtons: any = {
        leftButtons: [rightButton],
        rightButtons: []
    };
    static navigatorStyle = {
        navBarNoBorder: true,
        navBarTextColor: R.color.new_base_dark,
        navBarBackgroundColor: R.color.new_white,
        navBarButtonColor: R.color.new_blue,
        navBarTextFontFamily: 'IBM Plex Sans',
        navBarTextFontSize: 22
    };

    constructor(props: IProps) {
        super(props);
        this.state = {
            anotherScreenOpen: false,
            isShowPicker: false,
            profile: {
                currencies: props.profile.currencies
            },
            modalVisible: false,
            devModalVisible: false,
            serverText: '',
            clickCounter: 0,
            lastUpdate: ''
        };
        props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    }


    onNavigatorEvent(event: any) {
        switch (event.id) {

            case 'back':
                this.props.navigator.pop();
                break;
            default:
        }
    }

    private onValueChanged(value: any) {
        const {profile} = this.props;
        profile.currencies = value;
        this.props.actions.saveProfile(profile);

    }

    render() {
        const {profile} = this.props;
        return (
            <View key={'settings'} style={styles.container}>
                <CurrencyFilterDetailViewNew
                    currencies={this.props.course.data}
                    selectedCurrency={profile.currencies}
                    isNotChange={true}
                    onValueChanged={value => this.onValueChanged(value)}
                />
            </View>

        );
    }

}

function mapStateToProps(state: IRootState) {
    return {
        profile: state.profile,
        course: state.course,
        app: state.app
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<IAction<any>>) {
    return {
        actions: bindActionCreators(profileActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsView);