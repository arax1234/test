import {ViewStyle, TextStyle, ImageStyle, Dimensions} from 'react-native';
import R from '../../resources/R';

const {width, height} = Dimensions.get('window');

class Styles {
    loading: ViewStyle = {
        width: width,
        height: height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: R.color.new_white
    };

    container: ViewStyle = {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        backgroundColor: R.color.new_white,
        paddingTop: 20
    };
    itemContainer: ViewStyle = {
        flexDirection: 'column',
        paddingBottom: 10,
        paddingHorizontal: 20,
        justifyContent: 'flex-start'
    };
    textName: TextStyle = {
        fontFamily: 'IBM Plex Sans',
        fontWeight: '400',
        fontStyle: 'normal',
        fontSize: 17,
        textAlign: 'left',
        color: R.color.new_base_dark
    };
    subContainer: ViewStyle = {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 20
    };
    text: TextStyle = {
        fontFamily: 'IBM Plex Sans',
        fontWeight: '300', fontStyle: 'normal',
        color: R.color.new_base_dark,
        fontSize: 14
    };

}

export const styles = new Styles();
