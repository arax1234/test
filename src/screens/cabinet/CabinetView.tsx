import React, {Component} from 'react';
import {Text, View, FlatList} from 'react-native';
import {Pagination} from 'react-native-snap-carousel';

import {connect} from 'react-redux';
import Redux, {bindActionCreators} from 'redux';

import {styles} from './CabinetView.styles';
import R from '../../resources/R';

import * as profileActions from '../../actions/profile.actions';
import * as courseAction from '../../actions/course.actions';
import {IRootState} from '../../interfaces/IRootState';
import {IAction} from '../../interfaces/IAction';
import {IProfile} from '../../interfaces/IProfile';
import {ViewWithNavigator} from '../../components/ViewWithNavigator';
import {IApp} from '../../interfaces/IApp';
import * as appActions from '../../actions/app.actions';
import {IAppActions} from '../../interfaces/IAppActions';
import {ICours} from '../../interfaces/ICours';
import {ICourseAction} from '../../interfaces/ICourseAction';
import Preloader from '../../components/Preloader';
import SplashScreen from 'react-native-splash-screen';

interface IProps extends ViewWithNavigator {
    appActions: IAppActions;
    app: IApp;
    course: ICours;
    courseAction: ICourseAction;
    profile: IProfile;
}

interface IState {
    course: ICours;
}

const buttons = {
    SETTING: 'settings'
};
const rightButton = {
    icon: R.image.new_setting,
    title: '',
    id: buttons.SETTING
};

class CabinetView extends Component<IProps, IState> {

    public static navigatorButtons: any = {
        leftButtons: [],
        rightButtons: [rightButton]
    };
    static navigatorStyle: any = {
        navBarTextColor: R.color.new_base_dark,
        navBarBackgroundColor: R.color.new_white,
        navBarButtonColor: R.color.new_base_dark,
        navBarNoBorder: true,
        navBarTextFontFamily: 'IBM Plex Sans',
        navBarTextFontSize: 22
    };

    constructor(props: IProps) {
        super(props);
        this.state = {
            course: this.props.course
        };
        props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
        this.props.appActions.initRealm();
    }

    componentWillReceiveProps(pr: IProps) {
        this.setState({course: pr.course});
    }

    componentDidMount() {
        SplashScreen.hide();
    }

    onNavigatorEvent(event: any) {

        if (event.type === 'NavBarButtonPress') {
            if (event.id === buttons.SETTING) {
                this.props.navigator.push({
                    screen: 'uis.Settings',
                    title: R.string.tabsNames.settings,
                    backButtonHidden: true
                });
            }
        }
    }

    getCourse(item: any) {
        const currentItem = this.props.course.data.find(it => it.CharCode === this.props.profile.currencies);
        let l = 1;
        if (currentItem) {
            l = currentItem.Nominal / currentItem.Value;
        }
        return (item.Value * l).toFixed(4);
    }

    renderItem(item: any) {
        return (<View style={styles.itemContainer}>
            <Text style={styles.textName}>{item.item.Name}</Text>
            <View style={styles.subContainer}>
                <Text style={styles.text}>{item.item.Nominal + ' ' + item.item.CharCode}</Text>
                <Text style={styles.text}>{this.getCourse(item.item) + ' ' + this.props.profile.currencies}</Text>
            </View>
        </View>);
    }

    render() {
        //const index = this.state.course.data.findIndex(it => it.CharCode === this.props.profile.currencies);
        const {data} = this.state.course;
        const arr = [];
        if (this.state.course.data.length > 0) {
            data.forEach(it => {
                if (it.CharCode !== this.props.profile.currencies) {
                    arr.push(it);
                }
            });
        }
        return (
            <View style={styles.container}>
                {
                    this.props.course.data.length === 0 && this.props.course.loading ?
                        (<View style={styles.loading}><Preloader simple={true}/></View>) :
                        (<View>
                            <FlatList
                                keyExtractor={i => i.Id}
                                data={arr}
                                renderItem={(item) => this.renderItem(item)}/>
                        </View>)
                }
            </View>
        );
    }
}

function mapStateToProps(state: IRootState) {
    return {
        app: state.app,
        course: state.course,
        profile: state.profile
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<IAction<any>>) {
    return {
        actions: bindActionCreators(profileActions, dispatch),
        appActions: bindActionCreators(appActions, dispatch),
        courseAction: bindActionCreators(courseAction, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CabinetView);
