import {Navigation} from 'react-native-navigation';
import {Store} from 'redux';

import {IRootState} from '../interfaces/IRootState';
import CabinetView from './cabinet/CabinetView';
import SettingsView from './settings/SettingsView';
import CourseView from './course//CourseView';

export function registerScreens(store: Store<IRootState>, Provider: any, stage: number) {
    Navigation.registerComponent('uis.CourseView', () => CourseView, store, Provider);
    Navigation.registerComponent('uis.CabinetView', () => CabinetView, store, Provider);
    Navigation.registerComponent('uis.Settings', () => SettingsView, store, Provider);
}
