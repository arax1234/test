/**
 * обработчик ошибок
 */
export function dataError(type, err) {
    return dispatch => {
        dispatch({ type });
    };
}
export function dataSuccess(data, extData = { params: null, action: null }) {
    return dispatch => {
        dispatch(data);
    };
}
//# sourceMappingURL=index.js.map