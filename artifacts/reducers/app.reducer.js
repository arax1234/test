import { APP_LOAD, INIT_REALM_START, INIT_REALM_SUCCESS, INIT_REALM_ERROR, VALIDATE_INTERNET_START, VALIDATE_INTERNET_SUCCESS, VALIDATE_INTERNET_ERROR, SET_SERVER_START, SET_SERVER_SUCCESS, SET_SERVER_ERROR, INIT_SERVER_START, INIT_SERVER_SUCCESS, INIT_SERVER_ERROR, SET_STAGE_ERROR, SET_STAGE_START, SET_STAGE_SUCCESS } from '../actionTypes/app.actionTypes';
import initialState from '../store/initialState';
export default function (state = initialState, action) {
    switch (action.type) {
        case APP_LOAD:
            return Object.assign({}, state, { loading: false });
        case INIT_REALM_START:
            return Object.assign({}, state);
        case INIT_REALM_SUCCESS:
            return Object.assign({}, state, { realm: action.payload });
        case INIT_REALM_ERROR:
            return Object.assign({}, state);
        case VALIDATE_INTERNET_START:
            return Object.assign({}, state);
        case VALIDATE_INTERNET_SUCCESS:
            return Object.assign({}, state, { internet: action.payload });
        case VALIDATE_INTERNET_ERROR:
            return Object.assign({}, state);
        case SET_SERVER_START:
            return Object.assign({}, state);
        case SET_SERVER_SUCCESS:
            return Object.assign({}, state, { server: action.payload });
        case SET_SERVER_ERROR:
            return Object.assign({}, state);
        case INIT_SERVER_START:
            return Object.assign({}, state);
        case INIT_SERVER_SUCCESS:
            return Object.assign({}, state, { server: action.payload });
        case INIT_SERVER_ERROR:
            return Object.assign({}, state);
        case SET_STAGE_START:
            return Object.assign({}, state);
        case SET_STAGE_SUCCESS:
            return Object.assign({}, state, { stage: action.payload });
        case SET_STAGE_ERROR:
            return Object.assign({}, state);
        default:
            return state;
    }
}
//# sourceMappingURL=app.reducer.js.map