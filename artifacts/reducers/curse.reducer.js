import initialState from '../store/initialState';
import { GET_CURSE_START, GET_CURSE_ERROR, GET_CURSE_SUCCESS, UPDATE_CURSE_ERROR, UPDATE_CURSE_START, UPDATE_CURSE_SUCCESS } from '../actionTypes/curse.actionType';
export default function (state = initialState.course, action) {
    switch (action.type) {
        case GET_CURSE_START:
            return Object.assign({}, state, { loading: true, data: [] });
        case GET_CURSE_SUCCESS:
            return Object.assign({}, state, { loading: false, data: action.payload.data });
        case GET_CURSE_ERROR:
            return Object.assign({}, state, { loading: false });
        case UPDATE_CURSE_START:
            return Object.assign({}, state, { loading: true });
        case UPDATE_CURSE_SUCCESS:
            return Object.assign({}, state, { loading: false, data: action.payload.data });
        case UPDATE_CURSE_ERROR:
            return Object.assign({}, state, { loading: false });
        default:
            return state;
    }
}
//# sourceMappingURL=curse.reducer.js.map