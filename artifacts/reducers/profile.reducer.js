import { SAVE_PROFILE_ERROR, SAVE_PROFILE_START, SAVE_PROFILE_SUCCESS, INIT_PROFILE_ERROR, INIT_PROFILE_START, INIT_PROFILE_SUCCESS } from '../actionTypes/profile.actionTypes';
import initialState from '../store/initialState';
export default function (state = initialState.profile, action) {
    switch (action.type) {
        case SAVE_PROFILE_START:
            return Object.assign({}, state, { loading: true });
        case SAVE_PROFILE_SUCCESS:
            return Object.assign({}, state, action.payload, { organizations: [], paging: 1, totalOrganization: 0, loading: false });
        case SAVE_PROFILE_ERROR:
            return Object.assign({}, state, { loading: false });
        case INIT_PROFILE_START:
            return Object.assign({}, state, { loading: true });
        case INIT_PROFILE_SUCCESS:
            return Object.assign({}, state, action.payload, { loading: false });
        case INIT_PROFILE_ERROR:
            return Object.assign({}, state, { loading: false });
        default:
            return state;
    }
}
//# sourceMappingURL=profile.reducer.js.map