var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { GET_CURSE_ERROR, GET_CURSE_START, GET_CURSE_SUCCESS } from '../actionTypes/curse.actionType';
import axios from 'axios';
export function getCurrentCourse() {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        dispatch({ type: GET_CURSE_START });
        try {
            const response = yield axios.get('https://www.cbr-xml-daily.ru/daily_json.js');
            if (response.data) {
                const array = Object.keys(response.data.Valute).map((item) => {
                    return response.data.Valute[item];
                });
                const obj = {
                    CharCode: 'RUB',
                    Nominal: 1,
                    ID: 'R00000',
                    Name: 'Российский рубль',
                    Previous: 1,
                    Value: 1
                };
                array.push(obj);
                dispatch({
                    type: GET_CURSE_SUCCESS,
                    payload: { data: array }
                });
            }
        }
        catch (e) {
            dispatch(GET_CURSE_ERROR, e);
        }
    });
}
//# sourceMappingURL=course.actions.js.map