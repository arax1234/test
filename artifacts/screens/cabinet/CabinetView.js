import React, { Component } from 'react';
import { Text, View, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { styles } from './CabinetView.styles';
import R from '../../resources/R';
import * as profileActions from '../../actions/profile.actions';
import * as courseAction from '../../actions/course.actions';
import * as appActions from '../../actions/app.actions';
import Preloader from '../../components/Preloader';
import SplashScreen from 'react-native-splash-screen';
const buttons = {
    SETTING: 'settings'
};
const rightButton = {
    icon: R.image.new_setting,
    title: '',
    id: buttons.SETTING
};
class CabinetView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            course: this.props.course
        };
        props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
        this.props.appActions.initRealm();
    }
    componentWillReceiveProps(pr) {
        this.setState({ course: pr.course });
    }
    componentDidMount() {
        SplashScreen.hide();
    }
    onNavigatorEvent(event) {
        if (event.type === 'NavBarButtonPress') {
            if (event.id === buttons.SETTING) {
                this.props.navigator.push({
                    screen: 'uis.Settings',
                    title: R.string.tabsNames.settings,
                    backButtonHidden: true
                });
            }
        }
    }
    getCourse(item) {
        const currentItem = this.props.course.data.find(it => it.CharCode === this.props.profile.currencies);
        let l = 1;
        if (currentItem) {
            l = currentItem.Nominal / currentItem.Value;
        }
        return (item.Value * l).toFixed(4);
    }
    renderItem(item) {
        return (React.createElement(View, { style: styles.itemContainer },
            React.createElement(Text, { style: styles.textName }, item.item.Name),
            React.createElement(View, { style: styles.subContainer },
                React.createElement(Text, { style: styles.text }, item.item.Nominal + ' ' + item.item.CharCode),
                React.createElement(Text, { style: styles.text }, this.getCourse(item.item) + ' ' + this.props.profile.currencies))));
    }
    render() {
        //const index = this.state.course.data.findIndex(it => it.CharCode === this.props.profile.currencies);
        const { data } = this.state.course;
        const arr = [];
        if (this.state.course.data.length > 0) {
            data.forEach(it => {
                if (it.CharCode !== this.props.profile.currencies) {
                    arr.push(it);
                }
            });
        }
        return (React.createElement(View, { style: styles.container }, this.props.course.data.length === 0 && this.props.course.loading ?
            (React.createElement(View, { style: styles.loading },
                React.createElement(Preloader, { simple: true }))) :
            (React.createElement(View, null,
                React.createElement(FlatList, { keyExtractor: i => i.Id, data: arr, renderItem: (item) => this.renderItem(item) })))));
    }
}
CabinetView.navigatorButtons = {
    leftButtons: [],
    rightButtons: [rightButton]
};
CabinetView.navigatorStyle = {
    navBarTextColor: R.color.new_base_dark,
    navBarBackgroundColor: R.color.new_white,
    navBarButtonColor: R.color.new_base_dark,
    navBarNoBorder: true,
    navBarTextFontFamily: 'IBM Plex Sans',
    navBarTextFontSize: 22
};
function mapStateToProps(state) {
    return {
        app: state.app,
        course: state.course,
        profile: state.profile
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(profileActions, dispatch),
        appActions: bindActionCreators(appActions, dispatch),
        courseAction: bindActionCreators(courseAction, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(CabinetView);
//# sourceMappingURL=CabinetView.js.map