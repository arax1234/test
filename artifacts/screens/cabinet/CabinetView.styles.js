import { Dimensions } from 'react-native';
import R from '../../resources/R';
const { width, height } = Dimensions.get('window');
class Styles {
    constructor() {
        this.loading = {
            width: width,
            height: height,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: R.color.new_white
        };
        this.container = {
            flexDirection: 'column',
            justifyContent: 'flex-start',
            backgroundColor: R.color.new_white,
            paddingTop: 20
        };
        this.itemContainer = {
            flexDirection: 'column',
            paddingBottom: 10,
            paddingHorizontal: 20,
            justifyContent: 'flex-start'
        };
        this.textName = {
            fontFamily: 'IBM Plex Sans',
            fontWeight: '400',
            fontStyle: 'normal',
            fontSize: 17,
            textAlign: 'left',
            color: R.color.new_base_dark
        };
        this.subContainer = {
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingLeft: 20
        };
        this.text = {
            fontFamily: 'IBM Plex Sans',
            fontWeight: '300', fontStyle: 'normal',
            color: R.color.new_base_dark,
            fontSize: 14
        };
    }
}
export const styles = new Styles();
//# sourceMappingURL=CabinetView.styles.js.map