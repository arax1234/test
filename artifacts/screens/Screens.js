import { Navigation } from 'react-native-navigation';
import CabinetView from './cabinet/CabinetView';
import SettingsView from './settings/SettingsView';
import CourseView from './course//CourseView';
export function registerScreens(store, Provider, stage) {
    Navigation.registerComponent('uis.CourseView', () => CourseView, store, Provider);
    Navigation.registerComponent('uis.CabinetView', () => CabinetView, store, Provider);
    Navigation.registerComponent('uis.Settings', () => SettingsView, store, Provider);
}
//# sourceMappingURL=Screens.js.map