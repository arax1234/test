import { Dimensions } from 'react-native';
import R from '../../resources/R';
const { height, width } = Dimensions.get('window');
class Styles {
    constructor() {
        this.container = {
            backgroundColor: R.color.new_background,
            paddingTop: 20,
            paddingHorizontal: 20,
            flexDirection: 'column',
            height: height
        };
        this.rowContainer = {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingBottom: 20
        };
        this.inputText = {
            height: 40,
            width: width - 40,
            borderColor: R.color.new_extra_light,
            borderWidth: 1,
            borderRadius: 10,
            paddingHorizontal: 20,
            backgroundColor: R.color.new_white,
            color: R.color.new_light
        };
    }
}
export const styles = new Styles();
//# sourceMappingURL=CourseView.styles.js.map