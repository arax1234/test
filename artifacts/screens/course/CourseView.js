import * as React from 'react';
import { Text, View, TextInput } from 'react-native';
import { connect } from 'react-redux';
import { styles } from './CourseView.styles';
import R from './../../resources/R';
import { CurrencyFilterDetailViewNew } from '../settings/currency/CurrencyFilterDetailsViewNew';
const buttons = {
    CANCEL: 'cancel',
    BOOKMARKS: 'bookmarks'
};
class CourseView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            course: this.props.course,
            selectedValue: '',
            changeValue: ''
        };
        props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }
    onNavigatorEvent(event) {
    }
    componentWillReceiveProps(pr) {
        if (pr.course.data.length > 0) {
            this.setState({ course: pr.course, selectedValue: pr.course.data[0].CharCode });
        }
    }
    onValueChanged(value) {
        this.setState({ selectedValue: value });
    }
    onChangeValue(value) {
        this.setState({ changeValue: value });
    }
    render() {
        const { course, profile } = this.props;
        let sum = 0;
        if (course.data.length > 0) {
            const current = course.data.find(it => it.CharCode === this.state.selectedValue);
            sum = this.state.changeValue ? current.Value * parseInt(this.state.changeValue) : 0;
        }
        const arr = [];
        course.data.forEach(it => {
            if (it.CharCode !== profile.currencies) {
                arr.push(it);
            }
        });
        return (course.data.length > 0 &&
            (React.createElement(View, { style: styles.container },
                React.createElement(View, { style: styles.rowContainer },
                    React.createElement(Text, null, profile.currencies),
                    React.createElement(CurrencyFilterDetailViewNew, { currencies: arr, selectedCurrency: this.state.selectedValue, isNotChange: false, onValueChanged: value => this.onValueChanged(value) })),
                React.createElement(View, { style: styles.rowContainer },
                    React.createElement(TextInput, { keyboardType: 'number-pad', style: styles.inputText, value: this.state.changeValue, onChangeText: (value) => this.onChangeValue(value) })),
                React.createElement(View, { style: [styles.rowContainer, { justifyContent: 'center' }] },
                    React.createElement(Text, null, sum.toFixed(4))))));
    }
}
CourseView.navigatorStyle = {
    navBarTextColor: R.color.new_base_dark,
    navBarBackgroundColor: R.color.new_background,
    navBarNoBorder: true,
    navBarTextFontFamily: 'IBM Plex Sans',
    navBarTextFontSize: 22,
    navBarButtonColor: R.color.new_blue
};
function mapStateToProps(state) {
    return {
        profile: state.profile,
        app: state.app,
        course: state.course
    };
}
function mapDispatchToProps(dispatch) {
    return {};
}
export default connect(mapStateToProps, mapDispatchToProps)(CourseView);
//# sourceMappingURL=CourseView.js.map