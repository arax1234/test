import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { styles } from './SettingsView.styles';
import R from '../../resources/R';
import { CurrencyFilterDetailViewNew } from './currency/CurrencyFilterDetailsViewNew';
import * as profileActions from '../../actions/profile.actions';
const buttons = {
    BACK: 'back'
};
const rightButton = {
    icon: R.image.new_chevronLeft_blue,
    title: '',
    id: buttons.BACK
};
class SettingsView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            anotherScreenOpen: false,
            isShowPicker: false,
            profile: {
                currencies: props.profile.currencies
            },
            modalVisible: false,
            devModalVisible: false,
            serverText: '',
            clickCounter: 0,
            lastUpdate: ''
        };
        props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }
    onNavigatorEvent(event) {
        switch (event.id) {
            case 'back':
                this.props.navigator.pop();
                break;
            default:
        }
    }
    onValueChanged(value) {
        const { profile } = this.props;
        profile.currencies = value;
        this.props.actions.saveProfile(profile);
    }
    render() {
        const { profile } = this.props;
        return (React.createElement(View, { key: 'settings', style: styles.container },
            React.createElement(CurrencyFilterDetailViewNew, { currencies: this.props.course.data, selectedCurrency: profile.currencies, isNotChange: true, onValueChanged: value => this.onValueChanged(value) })));
    }
}
SettingsView.navigatorButtons = {
    leftButtons: [rightButton],
    rightButtons: []
};
SettingsView.navigatorStyle = {
    navBarNoBorder: true,
    navBarTextColor: R.color.new_base_dark,
    navBarBackgroundColor: R.color.new_white,
    navBarButtonColor: R.color.new_blue,
    navBarTextFontFamily: 'IBM Plex Sans',
    navBarTextFontSize: 22
};
function mapStateToProps(state) {
    return {
        profile: state.profile,
        course: state.course,
        app: state.app
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(profileActions, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(SettingsView);
//# sourceMappingURL=SettingsView.js.map