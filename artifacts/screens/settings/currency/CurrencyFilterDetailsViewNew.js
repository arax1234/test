import React, { Component } from 'react';
import { Dimensions, Modal, Picker, Text, TouchableOpacity, View, Image } from 'react-native';
import R from '../../../resources/R';
export class CurrencyFilterDetailViewNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            opened: false,
            selectedValue: this.props.selectedCurrency
        };
    }
    render() {
        const { currencies, selectedCurrency } = this.props;
        const currency = currencies.find((item) => item.CharCode.toString() === selectedCurrency);
        const currencyName = currency ? currency.Name : '';
        return [
            React.createElement(TouchableOpacity, { style: styles.container, onPress: () => this.setState({ opened: !this.state.opened }) },
                React.createElement(View, { style: styles.subContainer }, this.props.isNotChange ?
                    [
                        React.createElement(Text, { style: styles.header }, 'Валюта'),
                        React.createElement(View, { style: styles.descriptionContainer }, (React.createElement(Text, { style: styles.descriptionBlack, numberOfLines: 1 }, currencyName))),
                        React.createElement(Image, { style: styles.chevronImage, source: R.image.new_chevronRight_blue })
                    ] : [
                    React.createElement(View, { style: styles.descriptionContainer },
                        React.createElement(Text, { style: styles.descriptionBlack, numberOfLines: 1 }, selectedCurrency)),
                    React.createElement(Image, { style: styles.chevronImage, source: R.image.new_chevronBottom_blue })
                ])),
            React.createElement(Modal, { animationType: 'fade', transparent: true, visible: this.state.opened },
                React.createElement(View, { style: styles.modalBackground, onTouchStart: () => this.setState({ opened: !this.state.opened }, this.props.onValueChanged(this.state.selectedValue ? this.state.selectedValue.toString() : null)) }),
                React.createElement(View, { style: styles.modalContainer },
                    React.createElement(View, { style: styles.buttonContainer },
                        React.createElement(TouchableOpacity, { style: styles.leftHeader, onPress: () => {
                            } }),
                        React.createElement(TouchableOpacity, { style: styles.rightHeader, onPress: () => {
                                this.setState({ opened: !this.state.opened }, this.props.onValueChanged(this.state.selectedValue ? this.state.selectedValue.toString() : null));
                            } },
                            React.createElement(Text, { style: styles.okText }, "\u0413\u043E\u0442\u043E\u0432\u043E"))),
                    React.createElement(View, { style: styles.modalList },
                        React.createElement(Picker, { selectedValue: this.state.selectedValue, onValueChange: (value) => {
                                this.setState({ selectedValue: value ? value.toString() : null });
                            } }, [
                            currencies.map(item => (React.createElement(Picker.Item, { key: item.Id, label: `${item.Name}`, value: item.CharCode ? item.CharCode : null })))
                        ]))))
        ];
    }
}
const { width } = Dimensions.get('window');
class Styles {
    constructor() {
        this.container = {
            marginHorizontal: 20,
            paddingVertical: 20,
            borderBottomWidth: 0.5,
            borderBottomColor: R.color.new_line_base
        };
        this.subContainer = {
            flexDirection: 'row',
            alignItems: 'center'
        };
        this.iconContainer = {
            width: 30
        };
        this.icon = {};
        this.header = {
            fontFamily: 'IBM Plex Sans',
            fontWeight: '400',
            fontStyle: 'normal',
            fontSize: 15,
            color: R.color.new_base_dark
        };
        this.descriptionContainer = {
            flexDirection: 'row',
            justifyContent: 'flex-end',
            marginRight: 18,
            flex: 1,
            paddingLeft: 10
        };
        this.description = {
            fontFamily: 'IBM Plex Sans',
            fontWeight: '400',
            fontStyle: 'normal',
            fontSize: 15,
            color: R.color.new_light
        };
        this.descriptionBlack = {
            fontFamily: 'IBM Plex Sans',
            fontWeight: '400',
            fontStyle: 'normal',
            fontSize: 15,
            color: R.color.new_base_dark
        };
        this.chevronImage = {};
        this.dateLabel = {
            fontFamily: 'IBM Plex Sans', fontWeight: '400', fontStyle: 'normal',
            marginTop: 12,
            marginBottom: 12
        };
        this.dateText = {
            fontFamily: 'IBM Plex Sans',
            fontWeight: '400',
            fontStyle: 'normal',
            height: 23,
            justifyContent: 'flex-start',
            fontSize: 17
        };
        this.dateIcon = {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0,
            borderWidth: 0,
            display: 'none'
        };
        this.dateInput = {
            fontFamily: 'IBM Plex Sans',
            fontWeight: '400',
            fontStyle: 'normal',
            borderWidth: 0,
            height: 23,
            justifyContent: 'flex-start',
            flexDirection: 'row'
        };
        this.chevron = {
            width: 18,
            height: 18,
            tintColor: R.color.new_light
        };
        this.priceContainer = {
            flexDirection: 'row'
        };
        this.list = {
            flex: 1
        };
        this.fieldItem = {
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            width: '50%',
            paddingRight: 8
        };
        this.singleHeader = {
            fontFamily: 'IBM Plex Sans',
            fontWeight: '400',
            fontStyle: 'normal',
            fontSize: 17
        };
        this.priceLabel = {
            fontFamily: 'IBM Plex Sans',
            fontWeight: '400',
            fontStyle: 'normal',
            fontSize: 15,
            color: R.color.new_light,
            marginRight: 3
        };
        this.priceLabelBlue = {
            fontFamily: 'IBM Plex Sans',
            fontWeight: '400',
            fontStyle: 'normal',
            fontSize: 15,
            color: R.color.new_light,
            marginRight: 3
        };
        this.priceInput = {
            fontFamily: 'IBM Plex Sans',
            fontWeight: '400',
            fontStyle: 'normal',
            fontSize: 17,
            borderBottomWidth: 0.5,
            borderBottomColor: '#C8C7CC',
            flex: 2,
            paddingRight: 21,
            paddingLeft: 10,
            paddingVertical: 3
            // height: 34
        };
        this.clear = {
            position: 'absolute',
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            top: 0,
            right: 0,
            width: 29,
            height: 29
        };
        this.clearImage = {
            width: 16,
            height: 16,
            marginRight: 8,
            marginBottom: 4
        };
        this.modalBackground = {
            position: 'absolute',
            backgroundColor: R.color.new_base_dark,
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            opacity: 0.5
        };
        this.modalContainer = {
            position: 'absolute',
            bottom: 0
        };
        this.buttonContainer = {
            width: width,
            backgroundColor: 'white',
            paddingHorizontal: 20,
            paddingVertical: 15,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end',
            borderBottomColor: R.color.new_line_base,
            borderBottomWidth: 1,
            borderTopColor: R.color.new_line_base,
            borderTopWidth: 1
        };
        this.leftHeader = {
            width: width - 100
        };
        this.rightHeader = {
            width: 60
        };
        this.modalList = {
            width: width,
            backgroundColor: 'white'
        };
        this.okText = {
            fontFamily: 'IBM Plex Sans',
            fontWeight: '500',
            fontStyle: 'normal',
            color: R.color.new_blue,
            fontSize: 15
        };
    }
}
const styles = new Styles();
//# sourceMappingURL=CurrencyFilterDetailsViewNew.js.map