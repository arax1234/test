import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
class Styles {
    constructor() {
        this.container = {
            flex: 1,
            flexDirection: 'column',
            height: height,
            width: width,
            paddingTop: 20
        };
    }
}
export const styles = new Styles();
//# sourceMappingURL=SettingsView.styles.js.map