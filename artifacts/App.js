var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import { registerScreens } from './screens/Screens';
import configureStore from './store/configureStore';
import initialState from './store/initialState';
import R from './resources/R';
export const store = configureStore(initialState);
function getApp() {
    console.disableYellowBox = true;
    Navigation.startTabBasedApp({
        tabs: [
            {
                label: R.string.tabsNames.cabinet,
                screen: 'uis.CabinetView',
                icon: R.image.new_user_dark,
                selectedIcon: R.image.new_user_blue,
                renderAsOriginal: true,
                title: R.string.tabsNames.cabinet,
                tabBarHideShadow: true
            },
            {
                label: R.string.tabsNames.home,
                screen: 'uis.CourseView',
                title: R.string.tabsNames.course,
                icon: R.image.new_search_dark,
                selectedIcon: R.image.new_search_new_blue,
                renderAsOriginal: true
            }
        ],
        tabsStyle: {
            initialTabIndex: 0,
            tabBarBackgroundColor: '#FFFFFF',
            tabBarTextFontFamily: 'IBM Plex Sans',
            tabBarLabelColor: R.color.new_base_dark,
            tabBarSelectedLabelColor: R.color.new_blue,
            tabBarHideShadow: true
        },
        animationType: 'fade'
    });
}
function startApp() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            getApp();
        }
        catch (error) {
        }
    });
}
export default function () {
    return __awaiter(this, void 0, void 0, function* () {
        registerScreens(store, Provider, 0);
        yield startApp();
    });
}
//# sourceMappingURL=App.js.map