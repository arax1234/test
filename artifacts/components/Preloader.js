/**
 * Прелодер с выводом ошибок (Отсутствия интернета и Неудачной попытки получить данные с сервера)
 * При нажатии повторить попытку пытается обновить данные списка и карточек закупок
 */
import React, { Component } from 'react';
import { View, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
class Preloader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notAllow: false
        };
        this.timerHandle = null;
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.internet && this.props.internet !== nextProps.internet) {
            this.handleAction();
        }
    }
    componentWillUnmount() {
        if (this.timerHandle) {
            clearTimeout(this.timerHandle);
            this.timerHandle = null;
        }
    }
    render() {
        return (React.createElement(View, { style: styles.container },
            React.createElement(ActivityIndicator, { hidesWhenStopped: true, color: '#000', style: { flex: 1 }, animating: true })));
    }
    reload() {
        this.setState({ notAllow: false }, () => {
            this.timeLimit();
        });
    }
    timeLimit() {
        if (this.timerHandle) {
            clearTimeout(this.timerHandle);
            this.timerHandle = null;
        }
        this.timerHandle = setTimeout(() => {
            this.setState({ notAllow: true });
        }, 20000);
    }
    handleAction() {
        this.reload();
    }
}
function mapStateToProps(state) {
    return {
        internet: state.app.internet
    };
}
function mapDispatchToProps(dispatch) {
    return {};
}
export default connect(mapStateToProps, mapDispatchToProps)(Preloader);
class Styles {
    constructor() {
        this.container = {
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            backgroundColor: 'transparent'
        };
        this.textContainer = {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            zIndex: 10
        };
        this.text = {
            fontFamily: 'IBM Plex Sans',
            fontWeight: '400',
            fontStyle: 'normal',
            paddingHorizontal: 50,
            textAlign: 'center',
            marginBottom: 20
        };
    }
}
const styles = new Styles();
//# sourceMappingURL=Preloader.js.map