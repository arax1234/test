import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/root.reducer';
import { composeWithDevTools } from 'remote-redux-devtools';
export default function configureStore(initialState) {
    return createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(thunk))
    //composeWithDevTools(applyMiddleware(thunk, logger))
    );
}
//# sourceMappingURL=configureStore.js.map