import R from '../resources/R';
const initialState = {
    app: {
        internet: true,
        loading: true,
        realm: undefined,
        server: R.server,
        stage: 0
    },
    course: {
        data: [],
        loading: true
    },
    profile: {
        currencies: 'RUB'
    }
};
export default initialState;
//# sourceMappingURL=initialState.js.map