#Памятка

##Установка

**В консоли выполнить команды**

- cd UIS

- yarn install

##Основные команды консоли

- npm run start-ios - запустить проект проект (либо подключенное устройство, либо эмулятор IPhone 6)

- npm run start-ios-se - запустить проект проект на эмуляторе IPhone SE

- npm run start-ios-x - запустить проект проект эмулятору IPhone X

- tsc - скомпилировать TypeScript в JavaScript

##Структура файлов папки _src_

- actions - содержит экшены redux
    - app.actions.ts - инициализация основных функций приложения
    - dictionaries.actions.ts - подключение словарей
    - favorites.actions.ts - экшены избранного
    - filters.actions.ts - экшены фильтров
    - filtersData.actions.ts - экшены полученя данных списка заказчиков
    - ktru.actions.ts - экшены полученя данных списка КТРУ 
    - profile.actions.ts - экшены профиля
    - purchase.actions.ts - экшены списка закупок
    - purchaseDetail.actions.ts - экшены карточки закупки

- actionTypes - содержит сонстанты всех экшенов
- client - обработчик ошибок
- components - содержит отдельные небольшие компоненты используемые на разных экрнах
    - BackButton.tsx - кнопка назад
    - EmptyView.tsx - компонент вывода экранного сообщения
    - HeaderView.tsx - кастомный header
    - MessageNotification.tsx - компонент вывода сообщений (тип Alert)
    - NavigationBackButton.tsx - кастомная кнопка назад навигатора
    - Preloader.tsx - прелоадер
    - ViewWithNavigator.tsx - базовый интерфейс навигатора
- core - содержит узконаправленные функции
    - dateTime.ts - содержит фенкции получения дат 
    - FilterFormating.ts - преобразует данные фильтра в запрос к API
    - Formatting.ts - функция форматирования цены
    - getUtcOffsetByMoscow.ts - функция получения часового пояса карточек 44-ФЗ
    - openUrl.ts - функция открытия url в браузере
    - UUID.ts - UUID hash
- interfaces - содержит часто используемые интерфейсы и интерфейсы экшенов
- models - содержит модели получаемых данных
- parsers - содержит классы парсинга данных списка и карточек закупок
- realm - содержит схемы сохраняемых на устройстве данных
- reducers - содержит обработчики redux
- resource - папка ресурсов
    - messageCodeTypes.tsx - содержит константы кодов ошибок
    - R.ts - содержит все ресурсы приложения (строки, цвета, подключения изображений, url адреса и др.)
- screens - содержит экраны приложения
    - another - экран другое
        - about - экран информации о приложении
        - components - дополнительные компоненты
        - feedback - экран "Оставить отзыв"
        - support - экран службы поддержки
    - favorites - экран избранное
        - FavoritesView.tsx - компонент вывода избранного 
    - filter - экран фильтры
        - filter-details - папка с компонентами конкретных фильтров
            - components
                - FilterToggleItem.tsx - обработчик переключателя
                - SearchField.tsx - компонент строки поиска
                - ToggleItemView.tsx - вывод переключателя
            - ClientFilterDetailView - экран фильтра по заказчику
            - CurrencyFilterDetailView - экран фильтра по валютам
            - DateFilterDetailView - экран фильтра по дате
            - KtruFilterDetailView - экран фильтра по КТРУ
            - LawFilterDetailView - экран фильтра по законам
            - PlacingWaysFilterDetailView - экран фильтра по способу определения поставщика
            - PriceFilterDetailView - экран фильтра по цене
            - PurchaseParticipantFilterDetailView - экран фильтра по участнику закупки 
            - RegionFilterDetailView - экран фильтра по субъекту РФ
            - SortFilterDetailView - экран сортировки (отключен)
            - StageFilterDetailView - экран фильтра по этапу закупки
        - filter-properties - папка с компонентами настройки фильтра
            - components
                - EditFilterItemView.tsx - компонент вывода элемента фильтра
            - CourseView - основной компонент настройки фильтра
        - FilterCell.tsx - компонент элемента списка фильтров
        - FilterListView.tsx - компонент списка фильтров
        - FilterParametersView.tsx - основной компонент подключения фильтров
    - profile - экран профиль
        - change-region - компонент выбора региона
        - change-status - компонент выбора статуса
        - ProfileView.tsx - основной компонент профиля
    - purchases - экран закупки
        - components
            - ActiveFilterIndicator.tsx - компонент вывода активного фильтра
            - PurchaseItemView.tsx - компонент вывода данных закупки
        - purchase-detail
            - components
                - MoreInformationContainer.tsx - компонет контейнера показывающего доп информацию по карточке закупки
                - PurchaseDetailsBottomBar.tsx - футер карточки закупки
            - TabCommonInfo - экран вкладки общая информация
            - TabDocs - экран вкладки документы
            - TabEvents - экран вкладки журнал событий
            - FavoritePurchaseDetailsView.tsx
            - PurchaseDetailsContentView.tsx
            - PurchaseDetailsView.tsx
        - PurchaseView.tsx - экран списка закупок
    - welcome - экран приветствия
    - Screens.ts - компонент подключения экранов


- store - хранилище redux
    - configureStore.ts - конфигурация хранилища
    - initialState.ts - начальное состояние хранилища
- App.tsx - основной файл приложения
- index.tsx - файл регистрации приложения

## Store ##

Содержимое Store можно посмотреть в **./src/store/initialStore.ts**
 
## Styles ##

Стили компонента должны лежать рядом с файлом компонента с идентичным названием и расширением .styles.ts

## Resources ##

Все ссылки на ресурсы приложения (изображения, цвета, строки) должны храниться в классе R